---
title: A powerful and open source content optimizer
category: software
description: Learn how I leveraged natural language processing technology to build a full-stack app that suggests improvements to your writing
image: optimizer-blog.png
tags: PHP, NLP, API, Web App
---

## Intro

The Article Optimizer was my first full-scale web app. I built it several years ago to familiarize myself with form-processing in PHP, and within a few months it had grown into a full-scale public web app which now enjoys usage by hundreds of unique visitors per month.