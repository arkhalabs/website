---
title: Premier article bien cool
category: software
description: Mon premier post !
tags: proud, dev, actu
---

## Intro

The Article Optimizer was my first full-scale web app. I built it several years ago to familiarize myself with form-processing in PHP, and within a few months it had grown into a full-scale public web app which now enjoys usage by hundreds of unique visitors per month.