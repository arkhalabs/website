const glob = require('glob');
const path = require('path');

const getDynamicRoutes = function () {
    return [].concat(
        glob
            .sync('*.md', {cwd: 'posts/'})
            .map((filepath) => `/blog/${path.basename(filepath, '.md')}`)
    )
};

const dynamicPaths = getDynamicRoutes();
console.log(dynamicPaths)

module.exports = {
    generate: {
        routes: dynamicPaths
    },
    mode: "universal",
    build: {
        extend(config, ctx) {
            config.module.rules.push({
                test: /\.md$/,
                include: [path.resolve(__dirname, 'posts'), path.resolve(__dirname, 'posts')],
                loader: 'frontmatter-markdown-loader'
            })
        }
    },
    plugins: [
        '~/plugins/common.js'
    ]
};